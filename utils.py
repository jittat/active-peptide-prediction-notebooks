from random import choice

def read_fasta(filename):
    seq = {}
    key = ''
    pep_lines = []
    for l in open(filename).readlines():
        l = l.strip()
        if l.startswith('>'):
            if key != '':
                seq[key] = ''.join(pep_lines)
                pep_lines = []            
            key = l.split()[0][1:]
            continue
        pep_lines.append(l)
    if key != '':
        seq[key] = ''.join(pep_lines)
    return seq

def random_seq(l=10):
    ALPHA = "ARNDCEQGHILKMFPSTWYVX"
    return ''.join([choice(ALPHA) for _ in range(l)])
